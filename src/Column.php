<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\grid;

/**
 * Column is the base class of all [[GridView]] column classes.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Column extends \yii\grid\Column
{
}
