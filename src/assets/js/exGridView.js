var obj = {
    init: function (options, element) {
        this.element = element
        this.$grid = $(this.element)

        var $this = this,
            checkbox = "input[name^='selection']:enabled",
            row = '.uk-table-hover tbody tr'

        $(document).off('click.yiiGridView', row).on('click.yiiGridView', row, function(e) {
            if (!$(e.target).is(":input, a, img")) {
                $(checkbox + ':checkbox', this).trigger('click');
            }
        });

        $(document).off('click.yiiGridView', checkbox).on('click.yiiGridView', checkbox, function(e) {
            $this.checkSelectedRows()
        });

        return this;
    },
    checkSelectedRows: function () {
        var keys = jQuery('#' + this.element.id).yiiGridView('getSelectedRows')

        $(this.element).find("input[name^='selection']:enabled").each(function (i, input) {
            $(input).closest('tr').toggleClass('uk-active', this.checked)
        })

        this.$grid.find('[grid-selected]').each(function () {
            $(this).toggleClass('uk-hidden', keys.length == 0)

            if (keys.length > 0) {
                var ids = {}
                $.each(keys, function (i, value) {
                    return ids['ids[' + i + ']'] = value
                })
                $(this).data('params', ids)
            }
        })
    }
};

if (typeof Object.create !== "function") {
    Object.create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}
$.plugin = function(name, object) {
    $.fn[name] = function(options) {
        return this.each(function() {
            if (!$.data(this, name)) {
                $.data(this, name, Object.create(object).init(options, this));
            }
        });
    };
};
$.plugin('exGridView', obj);
