<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\grid;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\Buttons;
use yii\widgets\Pjax;

/**
 * The GridView widget is used to display data in a grid.
 *
 * It provides features like [[sorter|sorting]], [[pager|paging]] and also [[filterModel|filtering]] the data.
 *
 * A basic usage looks like the following:
 *
 * ```php
 * <?= GridView::widget([
 *     'dataProvider' => $dataProvider,
 *     'columns' => [
 *         'id',
 *         'name',
 *         'created_at:datetime',
 *         // ...
 *     ],
 * ]) ?>
 * ```
 *
 * The columns of the grid table are configured in terms of [[Column]] classes,
 * which are configured via [[columns]].
 *
 * The look and feel of a grid view can be customized using the large amount of properties.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class GridView extends \yii\grid\GridView
{

    /**
     * @var array the configuration for the pager widget. By default, [[LinkPager]] will be
     * used to render the pager. You can use a different widget class by configuring the "class" element.
     * Note that the widget must support the `pagination` property which will be populated with the
     * [[\yii\data\BaseDataProvider::pagination|pagination]] value of the [[dataProvider]].
     */
    public $pager = [
        'class' => '\exoo\uikit\LinkPager',
        'options' => ['class' => 'uk-pagination uk-text-left']
    ];
    /**
     * @var array the HTML attributes for the summary of the list view.
     * The "tag" element specifies the tag name of the summary element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $summaryOptions = ['class' => 'summary uk-margin-small-top'];
    /**
     * @var string the default data column class if the class name is not explicitly specified when configuring a data column.
     * Defaults to 'yii\grid\DataColumn'.
     */
    public $dataColumnClass = 'exoo\grid\DataColumn';
    /**
     * @var array the HTML attributes for the grid table element.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $tableOptions = ['class' => 'uk-table uk-table-responsive uk-table-divider uk-table-hover uk-table-middle uk-table-small'];
    /**
     * Responsive Table
     *
     * @var boolean
     */
    public $overflowTable = true;
    /**
     * @var array the HTML attributes for the container.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $containerOptions = [];
    /**
     * @var string the layout that determines how different sections of the list view should be organized.
     * The following tokens will be replaced with the corresponding section contents:
     *
     * - `{summary}`: the summary section. See [[renderSummary()]].
     * - `{errors}`: the filter model error summary. See [[renderErrors()]].
     * - `{items}`: the list items. See [[renderItems()]].
     * - `{sorter}`: the sorter. See [[renderSorter()]].
     * - `{pager}`: the pager. See [[renderPager()]].
     */
    public $layout = "{header}\n{items}\n{summary}\n{pager}";
    /**
     * @var array the widget Buttons
     */
    public $buttons = [];
    /**
     * @var array the HTML attributes for the grid table element.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $buttonsOptions = [];
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $subnav;
    /**
     * @var array the HTML attributes for the grid table element.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $headerOptions = ['class' => 'uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle'];
    /**
     * @var string the property
     */
    public $modal = false;
    /**
     * @var string the property
     */
    public $modalOptions = [];
    /**
     * @var boolean the wrap pjax
     */
    public $pjax;
    /**
     * @var array the pjax options
     */
    public $pjaxOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();


        if ($this->modal) {
            $this->pjax = true;
        }
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['grid'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@exoo/grid/messages',
        ];
    }

    /**
     * Runs the widget.
     */
    public function run()
    {
        if ($this->pjax) {
            if (!isset($this->pjaxOptions['id'])) {
                $this->pjaxOptions['id'] = $this->options['id'] . 'Pjax';
            }
            Pjax::begin($this->pjaxOptions);
        }

        $view = $this->getView();
        GridViewAsset::register($view);
        $id = $this->id;
        $view->registerJs("jQuery('#$id').exGridView();");

        parent::run();

        if ($this->pjax) {
            Pjax::end();
        }
    }

    /**
     * @inheritdoc
     */
    public function renderItems()
    {
        $items = parent::renderItems();

        if ($this->overflowTable) {
            $items = Html::tag('div', $items, ['class' => 'uk-overflow-auto']);
        }

        if ($this->containerOptions) {
            $items = Html::tag('div', $items, $this->containerOptions);
        }

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{header}':
                return $this->renderHeader();
            case '{summary}':
                return $this->renderSummary();
            case '{items}':
                return $this->renderItems();
            case '{pager}':
                return $this->renderPager();
            case '{sorter}':
                return $this->renderSorter();
            default:
                return false;
        }
    }

    /**
     * Render buttons
     *
     * @return string the result
     */
    public function renderHeader()
    {
        if (empty($this->title) && empty($this->buttons)) {
            return;
        }

        $result = [];

        if (!empty($this->title)) {
            $result[] = Html::tag('div', $this->title, ['uk-margin' => true]);
        }

        if (!empty($this->buttons)) {
            $this->buttonsOptions['items'] = $this->buttons;
            $result[] = Buttons::widget($this->buttonsOptions);
        }

        $this->headerOptions['uk-margin'] = true;

        return Html::tag('div', implode("\n", $result), $this->headerOptions);
    }

    /**
     * @inheritdoc
     */
    public function renderCaption()
    {
        if (!empty($this->caption)) {
            $tag = ArrayHelper::remove($this->captionOptions, 'tag', 'caption');
            return Html::tag($tag, $this->caption, $this->captionOptions);
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function renderTableBody()
    {
        $models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach ($models as $index => $model) {
            $key = $keys[$index];
            if ($this->beforeRow !== null) {
                $row = call_user_func($this->beforeRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }

            $rows[] = $this->renderTableRow($model, $key, $index);

            if ($this->afterRow !== null) {
                $row = call_user_func($this->afterRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }
        }

        if (empty($rows) && $this->emptyText !== false) {
            $colspan = count($this->columns);

            return "<tbody>\n<tr><td colspan=\"$colspan\">" . $this->renderEmpty() . "</td></tr>\n</tbody>";
        }

        return "<tbody>\n" . implode("\n", $rows) . "\n</tbody>";
    }
}
