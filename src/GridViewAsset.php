<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\grid;

use yii\web\AssetBundle;

/**
 * Asset bundle for widget [[GridView]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class GridViewAsset extends AssetBundle
{
    public $sourcePath = '@exoo/grid/assets';
    public $js = [
        'js/exGridView.js',
    ];
    public $depends = [
        'yii\widgets\ActiveFormAsset',
        'exoo\kit\ExookitAsset',
        'yii\grid\GridViewAsset',
    ];
}
