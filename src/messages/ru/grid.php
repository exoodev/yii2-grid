<?php

return [
    'Move up by one position' => 'В начало по одной позиции',
    'Move down by one position' => 'В конец по одной позиции',
    'Move up' => 'Перемещение в начало',
    'Move down' => 'Перемещение в конец',
];
