<?php

namespace exoo\grid;

use yii\helpers\Html;

/**
 * SortableColumn.
 *
 * To add a SortableColumn to the [[GridView]], add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 *     // ...
 *     [
 *         'class' => 'exoo\grid\SortableColumn',
 *     ],
 * ]
 * ```
 *
 */
class SortableColumn extends Column
{
    /**
     * @var array the HTML attributes for the header cell tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $headerOptions = ['class' => 'uk-table-shrink'];

    /**
     * {@inheritdoc}
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return Html::tag('span', null, [
            'class' => 'uk-sortable-handle',
            'uk-icon' => 'icon: table'
        ]);
    }
}
