<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\grid;

use Yii;
use yii\helpers\Html;

/**
 * ActiveColumn.
 *
 * To add a ActiveColumn to the [[GridView]], add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 *     // ...
 *     [
 *         'class' => 'exoo\grid\ActiveColumn',
 *     ],
 * ]
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ActiveColumn extends DataColumn
{
    /**
     * @var string the attribute name associated with this column. When neither [[content]] nor [[value]]
     * is specified, the value of the specified attribute will be retrieved from each data model and displayed.
     *
     * Also, if [[label]] is not specified, the label associated with the attribute will be displayed.
     */
    public $attribute = 'active';
    /**
     * @var string|array|Closure in which format should the value of each data model be displayed as (e.g. `"raw"`, `"text"`, `"html"`,
     * `['date', 'php:Y-m-d']`). Supported formats are determined by the [[GridView::formatter|formatter]] used by
     * the [[GridView]]. Default format is "text" which will format the value as an HTML-encoded plain text when
     * [[\yii\i18n\Formatter]] is used as the [[GridView::$formatter|formatter]] of the GridView.
     * @see \yii\i18n\Formatter::format()
     */
    public $format = 'raw';
    /**
     * @var array the HTML attributes for the header cell tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $headerOptions = ['class' => 'uk-table-shrink'];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if ($this->filter === null) {
            $this->filter = [
                Yii::t('yii', 'No'),
                Yii::t('yii', 'Yes'),
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $icon = $model->active ? 'success' : 'muted';
        return Html::a(
            '<i class="fas fa-circle uk-text-' . $icon . '"></i>',
            ['active', 'id' => $model->id],
            [
                'data-pjax' => 0,
                'data-method' => 'post',
                'uk-tooltip' => Yii::t('system', 'Change status'),
                'class' => 'uk-button uk-button-small uk-button-default'
            ]
        );
    }
}
